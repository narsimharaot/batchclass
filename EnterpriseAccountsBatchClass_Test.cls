@isTest 
public class EnterpriseAccounts_Test 
{
    static testMethod void testMethod1() 
    {
        List<Account> lstAcc = new List<Account>();
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CustomerAccount').getRecordTypeId();
        for(Integer i=0 ;i <200;i++)
        {
            Account acc = new Account();
            acc.Name ='AccountName'+i;
            acc.RecordTypeId = devRecordTypeId;
            lstAcc.add(acc);
        }
        
        insert lstAcc;
        
        Test.startTest();

            EnterpriseAccounts obj = new EnterpriseAccounts();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
        
    }
}