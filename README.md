"# My project's README" 
Instructions to run the batch class:
There are two ways to run this batch class
	1. We can write a schedule class and schedule at a specific time. This option is used when we have to run the batch class more frequently.  
	2. Execute the code in the debug window of the dev console. We can use this option if it is a one time requirement.
			EnterpriseAccounts obj = new EnterpriseAccounts();
            DataBase.executeBatch(obj); //we can define the batch size in this step
			
Considerations
	1. I have used Database.QueryLocator as return type for the start method in the batch apex. This allows us to query upto 50 million records.
	2. We can use either custom settings or custom metadatatypes(my preferrence) to store the values for Enterprise_Account_Status__c field and use them in the code.
		Eg: Line 11 in batch class 
			acc.Enterprise_Account_Status__c = 'Bronze'; // Here instead of hardcoding the value 'Bronze' we can query for a custom metadatatype and use that value. So whenever the requirement changes we just change the value in the custom metadata type.

			