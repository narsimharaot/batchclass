global class EnterpriseAccounts implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CustomerAccount').getRecordTypeId();
        String query = 'SELECT Id, Enterprise_Account_Status__c FROM Account WHERE RecordTypeId =:devRecordTypeId';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        for(Account acc : scope){
            acc.Enterprise_Account_Status__c = 'Bronze'; 
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC){
    }
}

